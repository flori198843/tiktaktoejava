package gui;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class Audio {

	/* Audio for Button-click */	
	 
	private static void makeSound(String filename) {
		try {			
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(filename).getAbsoluteFile());
			Clip clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();		
		} catch (IOException | UnsupportedAudioFileException | LineUnavailableException ex) {		
			ex.printStackTrace();
		}
	}
	
	public static void buttonMakeSound() {
		String soundName = "audio/gdi_transmission_in.wav";  
		makeSound(soundName);
	}
	
	public static void makeWinnerSound() {
		String soundName = "audio/ah_yes.wav";  
		makeSound(soundName);
	}
	
}
