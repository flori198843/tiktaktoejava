package gui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

import theGame.GameField;
import theGame.Player;

public class GameFrame extends JFrame {
	
	private final static int cntGameButtonsRow = 3;
	private final static int cntGameButtonsColumn = 3;
	private JButton[][] gameButtons = new JButton[cntGameButtonsRow][cntGameButtonsColumn];
	private JButton newGame = new JButton();
	private JTextField info = new JTextField();
	private JTextField heading = new JTextField();
	
	/* Fonts */
	
	Font headingFont = new Font("Serif", Font.BOLD, 14);
	
	/* Game elements */
	
	private GameField gameField; 
	private Player player1; 
	private Player player2;
		
	public Player actPlayer; 
	
	/* Helping variables */
	
	private int row;
	private int column;
		
	/*
	 * Constructor
	 */
	
	public GameFrame(GameField gameField, Player player1, Player player2) {						
	
		/* Initializing game elements */ 
		
		this.gameField = gameField;
		this.player1 = player1;
		this.player2 = player2;
		
		actPlayer = player1;
		
		/* Atrributes of GameFrame*/	
		
		setVisible(true);	
		setSize(400, 375);	
		setTitle("TIK-TAK-TOE");
		setLayout(null);
		setResizable(false);		
		
		/* WindowListeners*/	 
		 
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		
		 /* Initialize components for GameFrame */
		
		createButtons();
		bindButtonsOnFrame();
		createAndBindTextfields();
		
		/* Initialize ActionListeners */
		
		addActionListenerstoGameButtons();
	}
	
	/* 
	 * Methods
	 */
	
	private void createAndBindTextfields() {
			
		info.setBounds(135, 260, 200, 20);
		info.setBackground(null);
		info.setBorder(null);	
		info.setEditable(false);
		info.setText("Player " +  actPlayer.getID() + " is on the move!");		
		
		heading.setBounds(60, 25, 350, 30);
		heading.setBorder(null);
		heading.setBackground(null);
		heading.setText("THIS IS TIK TAK TOE from Florian Matzeit");
		heading.setFont(headingFont);
		
		add(info);
		add(heading);
		
	}
	
	private void createButtons() {
		
		newGame.setBounds(155, 280, 100, 30);
		newGame.setText("NEW GAME");
		newGame.setVisible(false);
		
		for (int i = 0; i < cntGameButtonsRow; i++) {		 
			for (int j = 0; j < cntGameButtonsColumn; j++) {
				gameButtons[i][j] = new JButton();
				gameButtons[i][j].setVisible(true);
				gameButtons[i][j].setBounds(130+(j*50), 100+(i*50), 50, 50);
			}				
		}	
	}	
	
	private void bindButtonsOnFrame() {
		add(newGame);		
		for (int i = 0; i < cntGameButtonsRow; i++) {		 
			for (int j = 0; j < cntGameButtonsColumn; j++) {				
				add(gameButtons[i][j]);
			}				
		}	
	}
	
	private void togglePlayer() {
		if (actPlayer.equals(player1)) {
			actPlayer = player2;
		} else {
			actPlayer = player1;
		}
	}
	
	private void gameFinished() {
		
		for (int i = 0; i < cntGameButtonsRow; i++) {		 
			for (int j = 0; j < cntGameButtonsColumn; j++) {				
				gameButtons[i][j].setEnabled(false);;
			}				
		}	
		if (GameField.getWinnerFound()) {
			info.setText("PLAYER: " + actPlayer.getID() + " HAS WON!");
			Audio.makeWinnerSound();
		} else {
			info.setText("Game finished! No one has won!");
		}		
		info.setVisible(true);	
		newGame.setVisible(true);
	}
	
	private void resetGame() {
		GameField.resetGameField();
		newGame.setVisible(false);
		info.setText("");
		actPlayer = player1;
		for (int i = 0; i < cntGameButtonsRow; i++) {		 
			for (int j = 0; j < cntGameButtonsColumn; j++) {				
				gameButtons[i][j].setText("");
				gameButtons[i][j].setEnabled(true);
			}				
		}
	}
	
	private void addActionListenerstoGameButtons() {
		
		newGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resetGame();
			}
		});
		
		for (int i = 0; i < cntGameButtonsRow; i++) {
			for (int j = 0; j < cntGameButtonsColumn; j++) {	
				this.row = i;
				this.column = j;
				gameButtons[i][j].addActionListener(new ActionListener() {
					int alRow = row;
					int alColumn = column;
					public void actionPerformed(ActionEvent e) {							
						actPlayer.setSign(alRow, alColumn);	
						gameButtons[alRow][alColumn].setText(String.valueOf(actPlayer.getSign()));
						gameButtons[alRow][alColumn].setEnabled(false);
						if (GameField.getWinnerFound() || GameField.getSignMadeCnt() == 9) {
							gameFinished();
						} else {
						Audio.buttonMakeSound();
						togglePlayer();
						info.setText("Player " +  actPlayer.getID() + " is on the move!");
						}
					}
				});
			}			
		}		
	}		
}
