package theGame;

public class GameField {

	private static char[][] game;
	private static final int gameFieldSize = 3;
	private static final int gameIterSize = 4;
	private static boolean winnerFound = false;
	private static int signMadeCnt = 0;
	
	public GameField() {
		game = new char[gameFieldSize][gameFieldSize];		
	}
	
	public char[][] getGame() {
		return game;		
	}
	
	public static void resetGameField() {
		for (int i = 0; i < gameFieldSize; i++) {						
			for (int j = 0; j < gameFieldSize; j++) {						
				game[i][j] = '\0';
			}			
		}
		winnerFound = false;
		signMadeCnt = 0;
	}
	
	public int setGame(int row, int column, char sign){
		int err = 0;		
		
		if (row > 2 || column > 2 || row < 0 || column < 0) {
			System.err.println("Attention: Value for row AND column has to be between '1' and '3'!");
			err = -1;			
		} else if (game[row][column] != '\0') {
			System.err.println("Could not set sign! Table-Index is already set!");
			err = -1;
		} else {
			game[row][column] = sign;
			signMadeCnt++;
			if (prooveHorizontalVictory(row, column, sign) || prooveVerticalVictory(row, column, sign) || prooveCrossVictory(row, column, sign)) {
				if(GameMain.runInConsole) {System.out.println("################ WINNER ################");}
			}
		}	
		
		return err;
	}
	
	private boolean prooveHorizontalVictory(int row, int column, char sign) {			
		switch(column) {		
			case 0: 
				if (game[row][column] == game[row][column+1] && game[row][column] == game[row][column+2] ) {
					winnerFound = true;
				} 
				break;
				
			case 1:
				if (game[row][column] == game[row][column-1] && game[row][column] == game[row][column+1] ) {
					winnerFound = true;
				}
				break;
				
			case 2:	
				if (game[row][column] == game[row][column-1] && game[row][column] == game[row][column-2] ) {
					winnerFound = true;
				}
				break;			
		}					
		return winnerFound;
	}
	
	private boolean prooveVerticalVictory(int row, int column, char sign) {		
		switch(row) {		
			case 0: 
				if (game[row][column] == game[row+1][column] && game[row][column] == game[row+2][column] ) {
					winnerFound = true;
				} 
				break;
				
			case 1:
				if (game[row][column] == game[row-1][column] && game[row][column] == game[row+1][column] ) {
					winnerFound = true;
				}
				break;
				
			case 2:	
				if (game[row][column] == game[row-1][column] && game[row][column] == game[row-2][column] ) {
					winnerFound = true;
				}
				break;			
		}					
		return winnerFound;
	}
	
	private boolean prooveCrossVictory(int row, int column, char sign) {				
		
		if (row == column && row+column == 2) {
			if ((game[row][column] == game[row-1][column+1] && game[row][column] == game[row+1][column-1]) || (game[row][column] == game[row+1][column+1] && game[row][column] == game[row-1][column-1])) {
				winnerFound = true;
			} 					
		} else if (row+column == 0) {
			if (game[row][column] == game[row+1][column+1] && game[row][column] == game[row+2][column+2] ) {
				winnerFound = true;
			}
				
		} else if (row == column && row+column == 4) {				
			if (game[row][column] == game[row-1][column-1] && game[row][column] == game[row-2][column-2] ) {
				winnerFound = true;
			}	
			
		} else if (row == 0 && column == 2) {
			if (game[row][column] == game[row+1][column-1] && game[row][column] == game[row+2][column-2] ) {
				winnerFound = true;
			}				
		} else if (row == 2 && column == 0) {
			if (game[row][column] == game[row-1][column+1] && game[row][column] == game[row-2][column+2] ) {
				winnerFound = true;
			}		
		}	
		
		return winnerFound;
	}
	
	public static boolean getWinnerFound() {
		return winnerFound;		
	}
	
	public static int getSignMadeCnt() {
		return signMadeCnt;
	}	
	
	public String toString() {
		
		String out = "--------\n";
		for (int i = 1; i < gameIterSize; i++) {						
			for (int j = 1; j < gameIterSize; j++) {						
				if (j%3 != 0) {
					out = out + ("|" + game[i-1][j-1]);					
				} else {					
					out = out +("|" + game[i-1][j-1] + "|\n--------\n");
				}				
			}			
		}		
		return out;
	}
}
