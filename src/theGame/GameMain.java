package theGame;
import gui.GameFrame;
import java.util.Scanner;

public class GameMain {

	private static boolean quitGame = false;
	public static boolean runInConsole = false;
	
	private static void runGameInConsole(GameField gameField, Player p1, Player p2) {
		
		String firstInput;
		int row;
		int column;
		
		Scanner scanInput = new Scanner(System.in);
		int roundCnt = 1;
		Player player;
		
		System.out.println("*******************WELCOME TO MY TIKTAKTOE*******************\n"
						 + "                Powered by Florian Matzeit\n"
						 + "     Type first value for row and second value for column!\n"
						 + "Type a value between '1' and '3' or type 'exit' to quit the game!\n\n"  + gameField.toString()); 				          
		
		while (!GameField.getWinnerFound() && !quitGame) {	
			
			if (roundCnt%2 == 0) {
				player = p2;
			} else {
				player = p1;
			}			
			System.out.println("Spieler " + player.getID() + " ist am Zug!");					
			System.out.println("Bitte Zeile eingeben: ");
			firstInput = scanInput.next();
			
			if ( roundCnt == 9 || firstInput.equalsIgnoreCase("exit")){
				scanInput.close();				
				System.out.println("Game quit!");				
				quitGame = true;
			} else {
				row = Integer.parseInt(firstInput)-1;
				System.out.println("Bitte Spalte eingeben: ");
				column = Integer.parseInt(scanInput.next())-1;				
				if (player.setSign(row, column) == 0) {
					roundCnt++;
				}	
			}								
		}

	}
	
	public static void main(String[] args) {
		
		GameField gameField = new GameField();
		
		Player p1 = new Player(gameField);
		Player p2 = new Player(gameField);
		
		if (runInConsole) {
			runGameInConsole(gameField, p1, p2);
		} else {
			new GameFrame(gameField, p1, p2);
		}
		
	}

}
