package theGame;

public class Player {
	
	private int playerID = 1;
	private static int playerIDCnt;
	private static boolean firstSignUsed = false;
	private char sign;
	private GameField gameField;
	
		
	public Player(GameField gameField) {
		playerID = playerID + playerIDCnt;		
		playerIDCnt++;
		this.gameField = new GameField();
		if (!firstSignUsed) {
			sign = 'x';			
		} else {
			sign = 'y';
		}
		firstSignUsed = true;
	}
	
	public int setSign(int row, int column) {
		int nextTry = 0;
		if(gameField.setGame(row, column, sign) == 0) {
			if(GameMain.runInConsole) {System.out.println("Player " + playerID + " made his set:\n" + gameField.toString());}			
		} else {
			nextTry = -1;
		}
		return nextTry;
	}
	
	public int getID() {
		return playerID;		
	}
	
	public char getSign() {
		return sign;		
	}
}
